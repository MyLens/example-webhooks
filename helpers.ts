import LensApi, { Lens } from '@mylens/lens-api';
import { LENS_ENTERPRISE_PROJECT_ID, LENS_ENTERPRISE_PRIVATE_KEY } from './index';
// ------------ TYPES -------------//
// This determines how the lens was encrypted for the source. 
// for more infomation visit: https://gitlab.com/MyLens/lens-api#lensrequest
interface LensTarget {
    id: string;
    publicKey?: string;
    privateKey?: string;
}
interface LensReference {
    id: string; // ID that can be used to identitfy this Lens. 
    url: string; // URL to the Lens. 
    isSelfTarget: boolean; // https://gitlab.com/MyLens/lens-api#lensrequest
    target?: LensTarget; // See comments above. 
    state?: string; // NOT used. 
    created: number; // Date that this Lens was created. 
    ownerId: string; // The Blockstack ID of the Lens creator
    isRevoked: boolean; // Check to see if this Lens has been revoked. 
}
export enum WebhookAction {
    Create = 'create',
    Delete = 'delete',
    Update = 'update'
}
export interface WebhookPayload {
    url: string; // URL that the web hook should fire. 
    projectId: string; // Project ID used for fetching info about the project. 
    action: WebhookAction; // What should be done with the lens ref. 
    lensRef: LensReference; // The lens that triggered the webhook. 
    timestamp: number; // Time when webhook is fired. 
    attempt: number; // Currently not used. 
}
export interface ResolvedItem {
    name: string;
    value: string;
}
//------------END TYPES-------------//

//------------- Lens Helpers --------//
/**
 * Transform a Lens into a simple key-value pair.
 * @param  lens - Lens as returned by the Lens API.
 */
function transform(lens: any) {
    let lensCopy = Object.assign({}, lens);
    // Check for name mapping 
    if (lensCopy.remoteLens.nameMap && Object.keys(lensCopy.remoteLens.nameMap).length > 0) {
        let keys = Object.keys(lensCopy.remoteLens.nameMap);
        keys.forEach(k => {
            // reassign the name
            let data = lensCopy.remoteLens.data.find((x: any) => x.id === k);
            data.name = lensCopy.remoteLens.nameMap[k];
        });
    }
    let transformed = lensCopy.remoteLens.data.map((d: any) => {
        return { name: d.name, value: d.data };
    });
    transformed.push({ name: "lensID", value: lensCopy.remoteLens.id });
    return transformed;
}
// This API is used to decrypt Lenses. 
export const api = new LensApi({ clientId: LENS_ENTERPRISE_PROJECT_ID, privateKey: LENS_ENTERPRISE_PRIVATE_KEY });
/**
 * Attempt to decrypt and organize the data in a Lens using the NameMap and api.
 * @param lensRef
 */
export async function resolveLens(lensRef: LensReference): Promise<Array<ResolvedItem>> {
    let result: Lens;
    // Check to see if the Lens contains the key to open it. 
    if (lensRef.target && lensRef.target.privateKey) {
        result = await api.resolveLens({ url: lensRef.url, privateKey: lensRef.target.privateKey });
    }
    else {
        // Try to use our private key to open if not. 
        result = await api.resolveLens({ url: lensRef.url, privateKey: LENS_ENTERPRISE_PRIVATE_KEY });
    }
    // console.log('Plain result = ', result); 
    return transform(result);
}
//--------End Helpers-------------//
