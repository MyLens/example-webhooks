# Example Webhooks
This repo shows you how to use webhooks in your own project

## Installation
- `git clone https://gitlab.com/MyLens/example-webhooks.git`
- `cd example-webhooks`
- `npm install`

## Running 
- `npm run start` 

## Instructions for setting up Lens Management System (LMS)
Before you can start using web hooks, you need to be an owner of a project. 
To see if you have access to any projects, login to [LMS](lms.mylens.io) and 
navigate to your projects. If you are a first time user (i.e. you haven't setup 
a project and no one in your company has) then you need to contact support@mylens.io. 
We will happiliy create a new project for you. If you are a new user to an existing project, 
make sure that you have the owner of the project create an account for you to administer 
your project. 

### Step one
Navigate to your projects "Integrations" tab as pictured below. 


<img src="/integration.png" width=300px></img>


### Step two 
Enter the URLs for your webhooks for each action. You can use the same webhook for each 
or use a new webhook for each action, it is entirely up to you. Additionally, if you want 
to call more than one webhook per action, you can add more by comma separating them. See the graphic below. 

<img src="/filled_hooks.png" width=300px></img>


### Step Three (optional)
If you want to test your webhooks locally, all you have to do is run the project (make sure you are running on port 3000)
and then tunnel the connection out using something like Ngrok. Here are the steps: 
1) `npm run start` This should be running on local port 3000
2) Download and install [NGROK](https://ngrok.com/).
3) run ngrok: `ngrok http 3000`
4) Get the URL for https displayed on your terminal, and use that as the webhook! 

Whenever anyone creates, deletes, or edits a Lens with your project, you should see the webhooks print
information about what changed. Happy coding! 
