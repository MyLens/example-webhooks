import express from 'express';
import cors from 'cors';

import bodyParser from 'body-parser';
import { webhookMiddleware } from '@mylens/lens-enterprise-api';
import { WebhookPayload, WebhookAction, resolveLens } from './helpers';


// Make sure you have these variables set!
export const LENS_ENTERPRISE_PRIVATE_KEY = process.env.LENS_ENTERPRISE_PRIVATE_KEY; 
if (LENS_ENTERPRISE_PRIVATE_KEY === undefined) {
    throw new Error('You must provide the LENS_ENTERPRISE_PRIVATE_KEY environment variable to decrypt messages!'); 
}
export const LENS_ENTERPRISE_PROJECT_ID = process.env.LENS_ENTERPRISE_PROJECT_ID || ""; 
if (LENS_ENTERPRISE_PROJECT_ID === "") {
    throw new Error('You must provide the LENS_ENTERPRISE_PROJECT_ID environment variable to decrypt Lenses!'); 
}

const app = express();
app.use(cors()); 
app.use(bodyParser.json({ strict: false }));

// End point to use to check if the service is running. 
app.get('/', async (req, res) => {
    res.status(200).send('Welcome to my Lens Webhook App.')
}) 

/**
 * Receive the webhooks fired by users modifying their data. The middleware verifies 
 * signatures and also decrypts the payload. Without this, you must decrypt the data yourself. 
 * To view the details on the encryption process, see https://gitlab.com/MyLens/lens-crypto/tree/master/source/symmetric. 
 */
app.post('/lens-hooks', webhookMiddleware(LENS_ENTERPRISE_PRIVATE_KEY), async (req, res) => {
    const payload : WebhookPayload = req.body; 
    
    console.log(`Lens user "${payload.lensRef.ownerId}" perfomed action "${payload.action}" on their Lens!`)
    
    // A user shared a Lens with your company!
    if (payload.action == WebhookAction.Create){
        console.log('Somebody created a Lens and shared it with me!'); 
        let resolved = await resolveLens(payload.lensRef); 
        console.log('resolved (new) lens = ', resolved); 

    // A user who already shared a Lens with you, updated their data in the Lens!
    } else if (payload.action == WebhookAction.Update){
        console.log('Somebody updated their data!')
        let resolved = await resolveLens(payload.lensRef); 
        console.log('Resolved (updated) lens = ', resolved); 

    // A user revoked their Lens! 
    } else if (payload.action == WebhookAction.Delete){
        console.log('Somebody Revoked their Lens from me :-(. ')

    // Got an unexpected action! We currently only have three actions, but we add more in 
    // the future, so make sure you add this line, just in case. 
    } else {
        throw new Error(`Received an unxpected lens action: "${payload.action}"`)
    }
    res.status(200).send();
});

// Start our express server on port 3000. 
const port = 3000; 
app.listen(port, () => {
    console.log(`Starting express server on port ${port}.`)
})
